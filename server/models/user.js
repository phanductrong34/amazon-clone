const { GetBucketEncryptionOutputFilterSensitiveLog } = require('@aws-sdk/client-s3');
const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
 
const UserSchema = new Schema({
    name: String,
    email: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    address: {type: Schema.Types.ObjectId, ref: 'Address'}
});

/*
Skema.pre('save',..) là 1 middleware ở tầng skema - gần với db nhất, làm thao tác gì đó trước khi save document mới
nó nhận vào callback next, và chỉ tiến hành save khi hàm next() đc gọi
ta ko dùng arrow funtion để truy cập đc biến thís, ở đây chính là document của UserSchema
Đầu tiên check nếu user đc tạo mới hoặc đc đổi pass, thì tiến hành chạy middleware mã hoá
tạo Salt với n kí tự thành công thì hash pass đc gửi lên với salt và gán lại vào user rồi mới save
Nếu không thì cứ save bt vào Skema thôi
*/
UserSchema.pre('save',function(next){
    let user = this;
    if (this.isModified('password') || this.isNew){
        bcrypt.genSalt(10, function(err,salt) {
            if(err){
                return next(err)
            }

            bcrypt.hash(user.password, salt,null,function(err,hash){
                if(err){
                    return next(err);
                }
                user.password = hash;
                next();
            })
        })
    } else{
        return next();
    }
});

// vì khi lưu vào database mk đã đc mã hoá, nên cần có hàm check mk (client gửi lên) và mk lưu ở skema (đc hash)
// định nghĩa 1 hàm cho data user đc trả về
UserSchema.methods.comparePassword = function (password, next){
    let user = this;
    return bcrypt.compareSync(password, user.password);
}
module.exports = mongoose.model("User", UserSchema);

