// const aws = require("aws-sdk");

const {S3Client} = require("@aws-sdk/client-s3")
const multer = require("multer");
const multerS3 = require("multer-s3");

// Thêm key để có thể truy cập và bucket S3

let s3 = new S3Client({
    credentials : {
        accessKeyId: process.env.AWSAccessKeyId,
        secretAccessKey: process.env.AWSSecretKey
    },
    region: 'ap-south-1'
})


//Hàm middleware uplaod ảnh vào S3 và trả về url đc sinh ra bởi fieldName của file trong req và time hiện tại
// Chính là thông tin đc định nghĩa trong metadata và key
const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'amazon-clone-trong',
        acl: 'public-read',
        metadata: (req,file,cb) => {
            cb(null,{fieldname: file.fieldname});
        },
        key: (req,file,cb) => {
            cb(null, Date.now().toString())
        }
    })
})

module.exports = upload;