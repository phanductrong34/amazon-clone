const router = require('express').Router();
const Product = require('../models/product');
const upload = require('../middlewares/upload-photo');
//POST request - create a new product

// title: String,
// description: String,
// photo: String,
// price: Number,
// stockQuantity: Number,
// rating: [Number]



//tương tự không khác gì app.post , nhưng cách viết này sau có thể import chồng vào URI của app
router.post('/products',upload.single("photo"), async(req, res) => {
    try{
        let product = new Product();
        product.categoryID = req.body.categoryID,
        product.ownerID = req.body.ownerID,
        product.title = req.body.title,
        product.description = req.body.description,
        product.photo = req.file.location,
        product.price = req.body.price,
        product.stockQuantity = req.body.stockQuantity,

        await product.save();

        res.json({
            success: true,
            message: "Successfully Saved Product"
        });

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })
    }
})


//GET request - get all products
router.get("/products", async(req, res) => {
    try{
        const products = await Product
                                .find()
                                .populate('owner category')
                                .exec();
        res.json({
            success: true,
            products : products
        })

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })
    }

})
//GET request - get a single products (which has field has exact value)
//"api/products/123h123kj12"
router.get("/products/:id", async(req, res) => {
    try{
        const product = await Product.findOne({_id: req.params.id}).populate("owner category").exec();
        res.json({
            success: true,
            product: product
        })

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })
    }

})


//PUT request - Update a single product
router.put("/products/:id",upload.single("photo"), async(req, res) => {
    try{
        const product = await Product.findOneAndUpdate(
            {_id: req.params.id}, 
            {
                $set: {
                    title: req.body.title,
                    description: req.body.description,
                    photo: req.file.location,
                    price: req.body.price,
                    owner: req.body.ownerID,
                    category: req.body.categoryID
                }
            },
            {upsert: true}   // tức nếu chưa có thì tạo mới luôn

        );
        res.json({
            success: true,
            updatedProduct: product
        })

    }catch(err){
        console.log(err.message);
        res.status(500).json({
            success: false,
            message: err.message
        })
    }
})
//DELETE request - Delete a single product
router.delete('/products/:id', async(req, res) => {
    try {
        let deletedProduct = await Product.findOneAndDelete(
            {_id: req.params.id}
        )
        if(deletedProduct){
            res.json({
                success: true,
                message: "Delete Product Successfully"
            })
        }

    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })
    }
})


module.exports = router;