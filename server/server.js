const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose= require('mongoose');
const dotenv = require('dotenv');
const User = require('./models/user');
const cors = require("cors");

//dotenv để tạo ra các biến môi trường nằm ngoài chương trình để giấu data nhạy cảm như key tới db
dotenv.config();

//morgan để log http res ra terminal, body parser để parse res ra format mình cần ( cả hai thằng này đều là middleware)

const app = express();

//Connect to mongoDB
mongoose.connect(
    process.env.DATABASE, 
    err=> {
    if(err){
        console.log(err);
    }else{
        console.log("Connected to database");
    }
})


//Middleware
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Require APIs (viết Api ở routes, sau đó import vào server)
const productRoutes = require('./routes/product');
const categoryRoutes = require('./routes/category');
const ownerRoutes = require('./routes/owner');
const userRoutes = require('./routes/auth')
app.use("/api", productRoutes);
app.use("/api", categoryRoutes);
app.use("/api", ownerRoutes)
app.use("/api",userRoutes)



//GET - Retreive data from server
app.get('/', (req,res) => {
    res.json("fetch success");
});

//POST - send data from frontend to backend
app.post('/',(req,res) => {
    let user = new User();
    user.name = req.body.name,
    user.email = req.body.email,
    user.password = req.body.password

    user.save(err => {
        if(err){
            res.json(err);
        }else{
            res.json("successfully save");
        }
    })
})

app.listen(3000, (err)=>{
    if(err){
        console.error(err);
    }else{
        console.log('Listening on PORT', 3000);
    }
})