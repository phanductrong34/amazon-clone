# Các Bước cài đặt lại app
Server
- Install nodemon bằng npm i -g nodemon
- Chạy nodemon server ở terminals
Admin
- Install nuxt-cli bằng npm i -g @nuxt/cli
- Chay app bằng npm run dev



# Các bước xây dựng app
- thêm folder server, cài npm express, viết file servser.js,các hàm get, post và listen để xử lý các rew gửi tới route gốc
- Cài postman và bắn thử req tới server localhost xem có đc trả vè không, cài thêm nodemon để tự reload server
- Tạo 1 project mongoDB và lấy accessKey thêm vào thành biến enviroment cho bảo mật, thêm hàm kết nối ở server
- Viết các models từ thư viện mongoose đóng vai trò như blueprint để thêm vào mongoDB sau này
- Viết routes đầu tiên của server là product.js, đón data từ body của req và dùng model tạo ra dữ liệu mới và lưu vào mongoDB


- Tạo 1 bucket trên AWS, tải về các key acccess và bucket và viết upload-photo middleware để khi server đón request từ client, sẽ thực hiện upload vào bucket của aws bằng multer, lấy về url và thêm vào model trước khi save vào mongoDB
(User upload photo => nhảy vào middleware trước (S3) process => url => lưu vào database)

- Viest route cho category để có category api, tạo ra category từ model của mongoose mỗi khi truy cập vào đường linh đó, ghi nhớ sau đó phải register route mới này vào server.js bằng app.use

- Tương tự viết route cho owner với post tạo owner mới và get toàn bộ owner từ mongo về

- Viết tất cả các hàm post + get (all, one) + put (one) cho route product

## Viết app admin 
- Tạo project nuxt và set nó chạy ở cổng 8000
  "config": {
    "nuxt": {
      "host": "0.0.0.0",
      "port": "8000"
    }
  },
  set trên script ở package.json trong folder admin

- tải các file style,font và ảnh ném vào static, ở file nuxt.js.config thêm tag css vào link để có thể xài đc file css ở toàn bộ app
=> Đây cũng là cách để tạo ra global css class mà có thể thêm vào style ở bất cứ chỗ nào trong app

-Code front end cho trang admin - index.js
- Gắn các nút để tương tác với backend api
- Sử dụng hàm asyncData để fetch api về trước khi app nuxt loading thành công => 1 hàm đặc biệt chỉ nuxt mới có, cái này hook create cũng ko làm đc, do có chạy trước thì app vẫn load nhanh hơn api về

### Trang thêm data phía admin

- product.vue => Trang thêm product từ phía front end
  - thêm vào folder Page => Nuxt sẽ tự thêm vào router
  - Code front end trước, các input và select option để thêm data sau này
  - sau khi có phần template, viết hàm asyncData(render ở serverside) để get về tất cả categories và owners cho vào hai mục select option (v-for) trước khi nó load lên
  - Viết hàm data (render client-sides) chứa tất cả các biến cần có ở model product để v-model map với input field ở trên
  - Viết methods onFileSelected để mỗi khi chọn file ở input thì gán vào biến fileName và selectedFile ở data
  - Viết methods onAddProduct gán vào click submit => tạo ra instance FormData và append data vào sau đó dùng axios post để bắn POST RESQUEST Với body kiểu form-data lên server, post thành công thì push router về / để redirect về home
  - Tuy nhiên sẽ bị lỗi cors, do browser chặn res trả về từ server không có cors (bảo về khỏi malicious file đc tải về ngầm), vào server.js install cors --save, thêm middleware app.use(cors) vậy là đc


- category.vue => thêm vào folder pages
  - Code front end, có phần list ở dưới để hiển thị các cate đã có rồi
  - Code hàm để xử lý nút submit, bắn REQ POST lên /api/categories, data trong body nạp vào chỉ là object (do không có ảnh), sau khi nhận RES thành công thì push data mới tạo vào list categories hiện tại
 
  - owners.vue => làm tương tự cái trên, để có thể tạo ra owner mới và nhìn tháy list ở ngay dưới

### Trang upadte data bên admin
- Update Product: tạo folder products trong pages, trong đó tạo index.vue (tự động nhận là routes /products luôn) và _id.vue (tự động nhận là /products/2342341)



# Front app phía client
- Tạo thư mục client với lệnh nuxt: npx create-nuxt-app client
- Chạy app client ở cổng 9000 bằng lệnh : PORT=9000 npm run dev => hoặc có thể set up ở script trong package.json, để khi chạy script "nuxt" thì nó tự nhảy PORT 9000

## Navbar cho toàn bộ app client
- Thêm Navbar.vue vào thư mục components
- Thêm thư mục layouts và viết default.vue, thêm <Nuxt /> cho app và <Navbar /> ở trên
- Copy Footer và paste vào thư mục component, import vào trong file layout/default
- vào trang https://shipow.github.io/searchbox/ để lấy search box đã đc code và copy vào component/Search.vue


## Trang index cho client
- Code front end cho phần best seller
- fetch data product về để hiển thị cho best seller, để ý thấy http://localhost:3000 được dùng đi dùng lại mỗi khi gọi axios, nên biến nó thành 1 biến tổng cho axios bằng cách vào nuxt.cònig.js thêm biến URL và ở property axios ở dưới thêm
axios: {
  proxy: true,
  baseURL : URL
}

- Fetch về tất cả data của product và v-for cho nó ở tất cả bestSeller, điền các thông tin chính xác vào đúng vị trí
- Tuy nhiên Categort và Author của sách đang ở dạgn ID => sửa lại ở bên phía server
- và routes products.js và populate MongoDB: ở hàm Product.find() ta thêm Productt.find().populate(owner category).exec()
thì sau khi lấy về res, nó sẽ tìm hai thuộc tính owner và category để phân giải cái ID thành object và trả lại vào đúng cái response


## Trang product cho client (detail và mua product)
- Tạo file product/_id.vue trong thư mục page của client => nũxt sẽ tự ren ra router
- Code front cho full trang detail ấy
- dùng asyncData({$axios,params}) để lấy data của đúng product có id ấy về
- Tuy nhiên hai property là owner và category là dạng ObjectID => viết ở bên server chỗ routes/product.js thêm vào hàm Product.findOne({_id: req.params.id}).populate("category owner").exec() để ngay ở phía backend 


## JSON WEB TOKEN 
- 1 loại container encryted để chứa các data nhạy cảm khi mà giao tiếp giữa hai agent với nhau (server - client)
- Vd gửi từ front username password lên server, trước khi đến server nó đi qua 1 cái middleware JWT để mã hoá thành 1 token, server sẽ trả lai 1 token ấy, và browsẻr ở client front giữ token đó ở local storage (cookie), sau này khi mà tạo request về user lên server, nó đính cái token và header ở request, req chạy qua middleware JWT để nó decode cái token, xác minh và đưa cho server phía sau để trả lại data cho client

## route Auth ở server

 THỦ TỤC
 Client -> bắn request tạo user -> server nhận data -> dừng biến SECRET và data sinh ra token trả về cho client
 Client -> bắn request lấy user -> servẻr nhận token trong header -> dùng biến SECRET so với token => nếu chuẩn thì trả về data đc giải mã từ token 


- tạo auth.js ở server/routes
- install các package cho middleware : npm i jsonwebtoken bcrypt-nodejs --save
- trong đó jsonwebtoken để mã hoá toàn bộ user document với 1 chuỗi string SECRET random để trả ra 1 cái token, còn bcrypt để mã hoá password user gửi lên
- Viết thêm vào server/models/user.js:
  - UserSkema.pre("save",..) để trước khi save vào skema thì check user document là mới, hay là đã bị thay đổi pass để hash lại password cùng salt đc gen random lại mỗi lần


- Viết route auth.js, đây là nơi đón req chứa email, password, và username, nếu data email, pass mà null thì báo res lỗi cho client, không thì add data mới vào blueprint User và save() => lúc này sẽ chạy middleware pre trước ở phía tầng SKema
- Sau khi save thành công, dùng jwt sign toàn bộ newUser với 1 chuỗi khoá giấu ở process.env để tạo ra token và gửi về cho client 

- Viết Middleware verify-token
  - viết ở server/middlewares/veryfy-token.js để đón token từ trong req của client lấy từ phía cookie của browser sau đó check token có hợp lệ
  - đầu tiên lấy token trong header ở mục x-access-token hoặc authorization
  - có khả năng token chứa string "Bearer " ở đầu nên cần xử lý cắt nó đi
  - phần token toàn kí tự còn lại cho đưa ra hàm jwt.verify để nó gỉai mã với biến SECRET ở server và so sánh với data trong req của client, nếu trùng khớp thì trae về cho client data đc decode đc thôi

=> vì SECRET dùng để sign lẫn verify (encode và decode) req của client
nên mới phjari giấu ở env, biến môi trường để ko lộ

### ROUTE GET user ở Auth.js
- viết hàm get đón request lấy thông tin user về, hàm này sẽ có middleware verify-token, để trích từ header ra token và
dùng hàm đối chiếu token với biến SECRET ở môi trường, và trả về dữ liệu đc decoded (hay chính là content đc giải mã từ token)

### ROUTE LOGIN ở Auth.js
- hàm post đón request từ client với body laf email, password
- Server lấy email trong body để findOne ra user tương ứng, nếu không có thì báo lỗi không có user
- nếu có thì check password bằng method tự tạo của userSkema là comparePassword (dùng bcrypt để check) nếu đúng thì gửi res, thành công
và 1 token đc kí lên foundUser kiếm đc
- Nếu password sai thì trả về res thất bại 


# FRONT signup để đăng nhập
- Tạo sinup.vue trong clients/pages và code giao diện cho nó
- Đây sẽ là nơi giao tiếp token giữa client và server, cần có 1 cơ chế đón token trả về sau khi request đăng nhập 
- npm i @nuxtjs/auth --save ở client là 1 cái module của nuxt, sau đó vào nuxt.config.js vào mmodules: "@nuxtjs/auth" để add thêm modules
- đồng thời config cả proxy và build trong file đó để set stratergies là local
https://auth.nuxtjs.org/schemes/local

# Nuxt/auth
- module của nuxt này, làm nhiệm vụ bên front, cung cấp APi để ta trigger mỗi khi làm authen,
để ta quản lý các thông tin về auth (đã đăng nhập chưa, data người dùng,token), mấy cái này
tự triển với vueX đc nhưng nó lằng nhàng, dùng cái này cho có hàm API, dùng cho tiện thôi,
 còn để xác minh tài khoản, sinh token là việc của server backend
Nhưng nó sẽ giúp mình tự động lưu token vào cookie và user data cùng cá biến auth quan trọng vào vueX

THỦ TỤC SIGN UP
USER -> ấn Signup -> data gồm name, email -> tới endpoint là /api/auth/signup -> 
server ở đó xử lý với middleware -> trả về token -> về /api/auth/login -> trigger Auth libary
-> đồng thời về /api/auth/user -> lưu token và data user về local Storage và VueX

THỦ TỤC LOGIN
Đơn giản là tới /api/auth/login và /api/auth/user và tiếp tục như trên thôi

# Cách hoạt động của hàm loginWith
do có setup bên nuxt.config.js
  build: {
    auth:{
      strategies: {
        local: {
          endpoints: {
            login:{ propertyName:"token"},
            logout: true
          }
        }
      }
    }
  }
-Sau khi response từ bên backend trả về là data cùng token, ta sử dụng hàm loginWith,
hàm này sẽ trigger enpoint login của Auth và tự động lấy biến token trong response về url hiện tại để lưu vào local storage, đồng thời lấy data của user đc trả về đáp vào vũe




